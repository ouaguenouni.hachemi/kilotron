# Morphogenesis for Robot Swarm

For our part, we have decided to concentrate our reflection on the Reaction-Diffusion mechanism presented in the article which inspired our project [there](https://robotics.sciencemag.org/content/3/25/eaau9178).

Reaction-Diffusion" models are models with continuous supports introduced by Alan Turing. He studied the appearance of visible patterns on the coats of certain species and modelled the evolution of two proteins, one which activates pigmentation and the other which inhibits it, by the following equations: 

<img src="https://render.githubusercontent.com/render/math?math=\partial_t q = D \Delta q + R(q)">

In the context of our study we will rely on the work of the article "Morphogenesis in robots swarm" which uses as reaction function for the activator and the inhibitor respectively the following two functions:

Activator:
<img src="https://render.githubusercontent.com/render/math?math=f(u,v)=(Au+Bv+C)-\gamma_u u ">


inhibitor:
<img src="https://render.githubusercontent.com/render/math?math=g(u,v)=(Eu - F)-\gamma_v v ">

You notice that theese two functions depends on several parameters, we tuned theese parameters with an evolutionnal algorithm in order to influence the behaviour of the swarm.
